<?php 
   session_start();
    if(empty($_SESSION['login'])){
      header("Location: nologin.php");
      exit;
    }
?>

<!DOCTYPE html>
<html  lang="ru">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="styles/styles.css">
    <link rel="stylesheet" href="styles/admin.css">
    <link type="image/x-icon" href="/images/favicon.ico" rel="shortcut icon">
    <link type="Image/x-icon" href="/images/favicon.ico" rel="icon">
    <script src="scripts/admin.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Административная панель</title>
  </head>
  <body>
  <header>
    <div class="container" style="width: auto; flex-direction: row;">
    <a href="/add.php" style="width: 200px;
      height: 100px; background: rgb(255, 228, 196); line-height: 4;">Добавить</a>
    <a href="index.php" style="width: 200px;
      height: 100px; background: rgb(255, 228, 196); line-height: 4;">На главную</a>
    <a href="/exit.php" style="width: 200px;
      height: 100px; background: rgb(255, 228, 196); line-height: 4;">Выход</a>

  </div>
  </header>
  <div class="flexbox arranged">
    <div class="list1"><script>print_list('.list1', 'ж');</script></div>
    <div class="list2"><script>print_list('.list2', 'м');</script></div>
    </div>
</body>
</html>