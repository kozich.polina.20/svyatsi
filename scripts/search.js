var url = '/scripts/names.json';
    fetch(url, {cache: "no-cache"})
    .then(res => res.json())
    .then(out =>
    {names = out;
    console.log('written successfully');
    localStorage.setItem('names', JSON.stringify(names));})
    .catch(err => { throw err });

function handle_search(e) {
    if (e.key === 'Enter'){
        const search_bar = document.getElementById('search');
        console.log(search_bar.value);
    }
}

function handle_search_button() {
    const search_bar = document.getElementById('search');
    const value = search_bar.value;
    let date_ = document.getElementById('dates');
    let name_ = document.getElementById('name');
    let value_ = document.getElementById('value');
    if (search_bar.value != ''){
        for (k = 0; k < names.length; k++) {
            if (names[k].name === value) {
                date_.textContent = names[k].day + ' ' + names[k].month;
                name_.textContent = names[k].name;
                value_.textContent = names[k].value;
                return;
            }
        }
        name_.textContent = 'Извините, имя не нашлось';
        date_.textContent = '';
        value_.textContent = '';
    }
}

