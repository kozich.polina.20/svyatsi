<!DOCTYPE html>
<html  lang="ru">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="styles/styles.css">
    <link rel="stylesheet" href="styles/indexstyles.css">
    <script src="scripts/calendar.js"></script>
    <link type="image/x-icon" href="/images/favicon.ico" rel="shortcut icon">
    <link type="Image/x-icon" href="/images/favicon.ico" rel="icon">
    <title>Календарь</title>
  </head>
  <body>
    <div class="wrapper">
      <header>
        <h1>Святцы</h1>
      <div>
        <a href="index.php">Главная</a>
        <a href="list.php">Список имен</a>
        <a href="search.php">Поиск имен</a>
      </div>
    </header>
    
    <div class="flexbox">
    <div class="calendar">
      <div class="month">
        <ul>
          <li class="prev" onclick="select_month_prev(); print_month();">&#10094;</li>
          <li class="next" onclick="select_month_next(); print_month();">&#10095;</li>
          <li>
            <div class="month_name"><script>print_month();</script></div>
            <span style="font-size:18px"></span>
          </li>
        </ul>
      </div>
      <ul class="days" id="ul/days">
      <script>print_days();
    </script>
      </ul>
    </div>
    <div class="description">
			Сайт "Святцы" - это справочник имен и дат именин. Для того, чтобы найти все имена, именины которых празднуются
        в определенный день месяца, найдите нужный Вам день в календаре и кликните на него. Перед Вами появится список имен, а также список соответствующих им значений.
        Для получения значения имени и даты именин воспользуйтесь либо списком имен, либо поиском. Получить информацию об имени через список можно кликнув на
         "Список имен", а затем на интересующее Вас имя. Для поиска нужного Вам имени кликните на "Поиск имен" и введите в строку поиска полное имя с заглавной буквы
         и нажмите кнопку "Поиск".
</div>
		</div>
  </div>
  </body>
</html>
