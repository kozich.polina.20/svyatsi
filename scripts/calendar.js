var url = '/scripts/names.json';
    fetch(url, {cache: "no-cache"})
    .then(res => res.json())
    .then(out =>
    {names = out;
    console.log('written successfully');
    localStorage.setItem('names', JSON.stringify(names));})
    .catch(err => { throw err });

const months = ['ЯНВАРЬ',
  'ФЕВРАЛЬ',
  'МАРТ',
  'АПРЕЛЬ',
  'МАЙ',
  'ИЮНЬ',
  'ИЮЛЬ',
  'АВГУСТ',
  'СЕНТЯБРЬ',
  'ОКТЯБРЬ',
  'НОЯБРЬ',
  'ДЕКАБРЬ'];

const month_duration = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

var i = 0;

function print_month() {
  let month_name = document.querySelector('.month_name');
  month_name.innerHTML = months[i] + "<br />";
};

function select_month_next() {
  if (i == 11) i = 0;
  else i++;
  remove_element();
  print_days();
};

function select_month_prev() {
  if (i == 0) i = 11;
  else i--;
  remove_element();
  print_days();
};

function get_month_duration() {
  return month_duration[i];
};

function print_days() {
  names = JSON.parse(localStorage.getItem('names'));
    if (names) {
    }
    else {
    fetch(url, {cache: "no-cache"})
    .then(res => res.json())
    .then(out =>
    {names = out;
    console.log('written successfully');
    localStorage.setItem('names', JSON.stringify(names));})
    .catch(err => { throw err });
    }
  const a31 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
  const a30 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
  const a29 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29];
  const list = document.querySelector('.days');
  if (get_month_duration() == 31) {
    a31.forEach(item => {
      const li = document.createElement('li');
      const button = document.createElement('button');
      var name_str = '';
      var value_str = '';
      for (k = 0; k < names.length; k++) {
        if (names[k].day === item.toString() && names[k].month === months[i]) {
          console.log(4);
          name_str += names[k].name + "<br />";
          value_str += names[k].value + "<br />";
        }
      }
      if (name_str === '') {
        button.addEventListener('click', function () {
          location.href = "dates.php?name=" + 'К сожалению, имени с такой датой не существует.';
        });
      } else {
        button.addEventListener('click', function () {
          location.href = "dates.php?name=" + name_str + '&' + 'day=' + item + '&' + 'month=' + months[i] + '&' + 'value=' + value_str;
        });
      }
      button.textContent = item;
      li.appendChild(button);
      list.appendChild(li);
    });
  } else
    if (get_month_duration() == 30) {
      a30.forEach(item => {
        const li = document.createElement('li');
        const button = document.createElement('button');
        var name_str = '';
        var value_str = '';
        for (k = 0; k < names.length; k++) {
          if (names[k].day === item.toString() && names[k].month === months[i]) {
            name_str += names[k].name + "<br />";
            value_str += names[k].value + "<br />";
          }
        }
        if (name_str === '') {
          button.addEventListener('click', function () {
            location.href = "dates.php?name=" + 'К сожалению, имени с такой датой не существует.';
          });
        } else {
          button.addEventListener('click', function () {
            location.href = "dates.php?name=" + name_str + '&' + 'day=' + item + '&' + 'month=' + months[i] + '&' + 'value=' + value_str;
          });
        }
        button.textContent = item;
        li.appendChild(button);
        list.appendChild(li);
      });
    } else {
      a29.forEach(item => {
        const li = document.createElement('li');
        const button = document.createElement('button');
        var name_str = '';
        var value_str = '';
        for (k = 0; k < names.length; k++) {
          if (names[k].day === item.toString() && names[k].month === months[i]) {
            name_str += names[k].name + "<br />";
            value_str += names[k].value + "<br />";
          }
        }
        if (name_str === '') {
          button.addEventListener('click', function () {
            location.href = "dates.php?name=" + 'К сожалению, имени с такой датой не существует.';
          });
        } else {
          button.addEventListener('click', function () {
            location.href = "dates.php?name=" + name_str + '&' + 'day=' + item + '&' + 'month=' + months[i] + '&' + 'value=' + value_str;
          });
        }
        button.textContent = item;
        li.appendChild(button);
        list.appendChild(li);
      });
    }
};

function remove_element() {
  let list = document.getElementById('ul/days').children;
  for (var i = list.length - 1; i >= 0; --i) {
    list[i].remove();
  }
};