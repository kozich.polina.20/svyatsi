<!DOCTYPE html>
<html  lang="ru">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="styles/liststyles.css">
    <link rel="stylesheet" href="styles/styles.css">
    <script src="scripts/list.js"></script>
    <link type="image/x-icon" href="/images/favicon.ico" rel="shortcut icon">
    <link type="Image/x-icon" href="/images/favicon.ico" rel="icon">
    <title>Список имен</title>
  </head>
  <body>
    <div class="wrapper">
      <header>
        <h1>Святцы</h1>
      <div>
        <a href="index.php">Главная</a>
        <a href="list.php">Список имен</a>
        <a href="search.php">Поиск имен</a>
      </div>
    </header>
    <div class="flexbox arranged">
    <div class="list1"><script>print_list('.list1', 'ж');</script></div>

    <div class="list2"><script>print_list('.list2', 'м');</script></div>
    </div>
  </div>
  </body>
</html>