<?php
    session_start();
    if (isset($_POST['password'])) {
        $password = $_POST['password'];
        $true_password = file_get_contents('hashes/hash.txt');
        if (password_verify($password, $true_password)) {
          $_SESSION['login'] = true;
          header('Location: admin_panel.php');
          } else {
            echo '<link type="image/x-icon" href="/images/favicon.ico" rel="shortcut icon">
            <link type="Image/x-icon" href="/images/favicon.ico" rel="icon">
            <p class="error"> Неверный пароль!</p>';
          }
      }
?>


<!DOCTYPE html>
<html  lang="ru">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="styles/styles.css">
    <link rel="stylesheet" href="styles/login.css">
    <link type="image/x-icon" href="/images/favicon.ico" rel="shortcut icon">
    <link type="Image/x-icon" href="/images/favicon.ico" rel="icon">
    <title>Войти</title>
  </head>
    <body>
    <form method="post" name="signin-form">
  <div class="form-element">
    <input type="password" name="password" required placeholder="Введите пароль" />
  </div>
  <button type="submit" name="login" value="login">Войти</button>
</form>
    </body>
</html>