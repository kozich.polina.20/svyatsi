<?php
Header("HTTP/1.1 404 Not Found");
?>

<!DOCTYPE html>
<html  lang="ru">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="styles/styles.css">
    <link type="image/x-icon" href="/images/favicon.ico" rel="shortcut icon">
    <link type="Image/x-icon" href="/images/favicon.ico" rel="icon">
    <title>Error 404</title>
  </head>
  <body>
    <div class="wrapper">
      <header>
        <h1>Святцы</h1>
      <div>
        <a href="index.php">Главная</a>
        <a href="list.php">Список имен</a>
        <a href="search.php">Поиск имен</a>
      </div>
    </header>
    </div>
    <div>
        Извините, страница не найдена!
    </div>
</body>
</html>