<!DOCTYPE html>
<html  lang="ru">

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="styles/styles.css">
  <link rel="stylesheet" href="styles/searchstyles.css">
  <script src="scripts/search.js"></script>
  <link type="image/x-icon" href="/images/favicon.ico" rel="shortcut icon">
  <link type="Image/x-icon" href="/images/favicon.ico" rel="icon">
  <title>Поиск имен</title>
</head>

<body>
  <header>
    <h1>Святцы</h1>
    <div>
      <a href="index.php">Главная</a>
      <a href="list.php">Список имен</a>
      <a href="search.php">Поиск имен</a>
    </div>
  </header>
  <div class="flexbox">
  <input type="search" name="search" id="search" placeholder="Введите полное имя с БОЛЬШОЙ буквы" onkeyup="handle_search(event);">
  <button type="submit" value="Поиск" id="searchButton" onclick="handle_search_button();">Поиск</button>
  </div>

  <div class="flexbox">
    <aside id="aside1" style="width: 300px">
      <h2>Имя</h2>
      <p id="name">
        <script></script>
      </p>
    </aside >
    <aside id="aside2" style="width: 300px">
      <h2>Даты именин</h2>
      <p id="dates">
        <script></script>
      </p>
    </aside>
    <aside id="aside3" style="width: 300px">
      <h2>Значение имени</h2>
      <p id="value">
        <script></script>
      </p>
</aside>
  </div>
</body>

</html>