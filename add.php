<?php
session_start();
    if(empty($_SESSION['login'])){
      header("Location: nologin.php");
      exit;
    }

    if(isset($_POST['done'])) {
      $json = file_get_contents('C:\OSPanel\domains\localhost\scripts\names.json');
      $names = json_decode($json, true);
      $new_name = array();
      $new_name['name'] = $_POST['name'];
      $new_name['sex'] = $_POST['sex'];
      $new_name['value'] = $_POST['value'];
      $new_name['day'] = $_POST['day'];
      $new_name['month'] = $_POST['month'];
      for ($i = 0; $i < count($names); $i++) {
        if ($names[$i]['name'] == $new_name['name']) {
            $error = 'Такое имя уже существует!';
            break;
        }
    }
    if (!isset($error)) {
      $names[] = $new_name;
      $json = json_encode($names, JSON_UNESCAPED_UNICODE);
      file_put_contents('C:\OSPanel\domains\localhost\scripts\names.json', $json);
    }
    else {
      echo '<link type="image/x-icon" href="/images/favicon.ico" rel="shortcut icon">
            <link type="Image/x-icon" href="/images/favicon.ico" rel="icon">
            <p class="error">Такое имя уже существует!</p>';
    }
  }
?>

<!DOCTYPE html>
<html  lang="ru">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="styles/styles.css">
    <link rel="stylesheet" href="styles/admin.css">
    <link rel="stylesheet" href="styles/login.css">
    <link type="image/x-icon" href="/images/favicon.ico" rel="shortcut icon">
    <link type="Image/x-icon" href="/images/favicon.ico" rel="icon">
    <title>Добавить</title>
  </head>
  <body>
  <header style="background: rgb(255, 228, 196);">
  <a href="admin_panel.php" style="width: 200px;
      height: 100px; background: rgb(246, 198, 139); line-height: 4;">Назад</a>
  </header>
  <form method="post" name="signin-form">
  <div class="form-element">
  <label>Имя</label>
    <input type="text" name="name" id="name" required/>
  </div>
  <div class="form-element">
  <label>Пол</label>
  <select id="sex" name="sex">
    <option value="ж">ж</option>
    <option value="м">м</option>
  </select>
  </div>
  <div class="form-element">
  <label>Значение имени</label>
    <input type="text" name="value" id="value" required/>
  </div>
  <div class="form-element">
  <label>День</label>
  <select id="day" name="day">
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
    <option value="24">24</option>
    <option value="25">25</option>
    <option value="26">26</option>
    <option value="27">27</option>
    <option value="28">28</option>
    <option value="29">29</option>
    <option value="30">30</option>
    <option value="31">31</option>
  </select>
  </div>
  <div class="form-element">
  <label>Месяц</label>
  <select id="month" name="month">
    <option value="ЯНВАРЬ">ЯНВАРЬ</option>
    <option value="ФЕВРАЛЬ">ФЕВРАЛЬ</option>
    <option value="МАРТ">МАРТ</option>
    <option value="АПРЕЛЬ">АПРЕЛЬ</option>
    <option value="МАЙ">МАЙ</option>
    <option value="ИЮНЬ">ИЮНЬ</option>
    <option value="ИЮЛЬ">ИЮЛЬ</option>
    <option value="АВГУСТ">АВГУСТ</option>
    <option value="СЕНТЯБРЬ">СЕНТЯБРЬ</option>
    <option value="ОКТЯБРЬ">ОКТЯБРЬ</option>
    <option value="НОЯБРЬ">НОЯБРЬ</option>
    <option value="ДЕКАБРЬ">ДЕКАБРЬ</option>
  </select>
  </div>
  <button type="submit" name="done" value="done">Готово</button>
</form>
</body>
</html>