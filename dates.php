<?php
$name = $_GET["name"];
$day = $_GET["day"] ?? "";
$month = $_GET["month"] ?? "";
$value = $_GET["value"] ?? "";
?>

<!DOCTYPE html>
<html  lang="ru">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/styles/styles.css">
    <link rel="stylesheet" href="/styles/datesstyles.css">
    <link type="image/x-icon" href="/images/favicon.ico" rel="shortcut icon">
    <link type="Image/x-icon" href="/images/favicon.ico" rel="icon">
    <title>Даты именин</title>
  </head>
  <body>
    <div class="wrapper">
      <header>
        <h1>Святцы</h1>
      <div>
        <a href="index.php">Главная</a>
        <a href="list.php">Список имен</a>
        <a href="search.php">Поиск имен</a>
      </div>
      </header>
    </div>
    <div class="flexbox">
		<aside id="aside1" style="width: 300px">
      <h2>Имя</h2>
      <p id="name"><?php echo $name ?></p>
    </aside>
    <aside id="aside2" style="width: 300px">
      <h2>Даты именин</h2>
      <p id="dates"><?php echo $day.' '.$month ?></p>
    </aside>
    <aside id="aside3" style="width: 300px">
          <h2>Значение имени</h2>
        <p id="value"><?php echo $value; ?></p>
</aside>
    </div>
  </body>
</html>